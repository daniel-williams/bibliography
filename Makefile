DESCRIPTION = "This is a Makefile for my Thesis"
TEX = pdflatex -shell-escape -interaction=nonstopmode -file-line-error
TEXMK = latexmk -f -pdf -pdflatex="pdflatex -interaction=nonstopmode"  #-use-make
#TEXMK = latexmk -f -pdf -pdflatex="xelatex -interaction=nonstopmode"  #-use-make
BIB = bibtex
PRE =  $(TEX) -ini -job-name="preamble" "&pdflatex preamble.tex\dump"

BASH = /bin/bash
PANDOC = pandoc
SUBBER = $(BASH) scripts/build/replace-abb.sh

BDIR = _build

ORG_BIB=$(wildcard *.org)
INT_BIBS=$(ORG_BIB:.org=.intbib)
BIB_FILES=$(ORG_BIB:.org=.bib)
#$(patsubst chapters/%.org,$(BDIR)/org/%.int_tex,$(ORG_FILES))
#TEX_FILES=$(patsubst chapters/%.org,$(BDIR)/tex/%.tex,$(ORG_FILES))
HTML_FILES=$(patsubst chapters/%.org,$(BDIR)/html/%.html,$(ORG_FILES))
.PHONY: document.pdf all clean glossary pdf help tex figures scripts

help :  ## Show this help message.
# This code snippet came from https://gist.github.com/prwhite/8168133
	@echo $(DESCRIPTION)
	@echo "---"
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

bib : $(BIB_FILES)

%.intbib : %.org
	$(SUBBER) $< > $@
%.bib : %.org
	echo "Producing $@ at $(notdir $@)"
	emacs $< -Q --batch --eval "(require 'org-bibtex)"   --eval '(org-bibtex "$(notdir $@)")' --kill

#glossary : tex/glossaries.tex ## Convert the glossary into a format acceptable to one of the languages supported.

clean:	## Remove all of the temporary files which the various compilation steps produce
	latexmk -CA
	cd scripts && make clean && cd -
	rm $(TEX_FILES)
	rm $(INT_FILES)
	rm -rf *.glo *.glg *.ist *.acn *.xdy *.acr *.alg
	rm -rf *.bbl *.gls *.glsdefs *.bib *.int_bib
	rm -rf *.mtc*
